import random
import math
import timeit
import csv

def insertionSort(arr):
    for i in range(1, len(arr)): 
        key = arr[i] 
        j = i-1
        while j >= 0 and key < arr[j] : 
                arr[j + 1] = arr[j] 
                j -= 1
        arr[j + 1] = key
    return arr

def quicksort_copy(L):
    if len(L) < 2:
        return L
    pivot = L[0]
    left, right = [], []
    for num in L[1:]:
        if num < pivot:
            left.append(num)
        else:
            right.append(num)
    return quicksort_copy(left) + [pivot] + quicksort_copy(right)

def test():
    with open('aaalister4422ee.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["n", "Run time"])
        #totalTime = 0
        for i in range(150):
            x = random.sample(range(150), i)
            start = timeit.default_timer()
            quicksort_copy(x)
            end = timeit.default_timer()
            writer.writerow([i, end - start])
        
test()
