import random
import math
import timeit
import csv
from sorts import selectionSort
from sorts import my_quicksort
from sorts import bubbleSort
from sorts import insertionSort
from sorts import create_random_list
from sorts import quicksort_copy

def nearlySortedTest():
    with open('aaalister4422ee.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["n", "Run time"])
        #totalTime = 0
        for i in range(150):
            x = random.sample(range(150), i)
            start = timeit.default_timer()
            quicksort_copy(x)
            end = timeit.default_timer()
            writer.writerow([i, end - start])
        
def timeTestInPlace(runs,L):
    total = 0
    for i in range(runs):
        start = timeit.default_timer()
        quicksort_inplace(L) #Manually change this for each test
        end = timeit.default_timer()
        total += end - start
    return total/runs

def timeTest(runs,L,type): 
    total = 0
    for i in range(runs):
        start = timeit.default_timer()
#        my_quicksort(L, type) #Manually changed type of sort for each test
        end = timeit.default_timer()
        total += end - start
    return total/runs

def testManually(): #code for generating all graphs involving the average case

    with open('testingInPLace.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["n", "selection"])
        for i in range(0,101,1):
            L = create_random_list(i)
            writer.writerow([i, timeTest(10,L,1)])

        return

def create_near_sorted_list1(n, factor,L):
    L.sort()
    for _ in range(math.ceil(len(L) * factor)):
        index1 = random.randint(0, len(L) - 1)
        index2 = random.randint(0, len(L) - 1)
        L[index1], L[index2] = L[index2], L[index1]
    return L

def worstCasetest():
    with open('tttttooooddd555.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["factor", "Run time"])
        #totalTime = 0
        x = random.sample(range(1000), 1000)
        b = 0
        for i in range(1,100):
            a = create_near_sorted_list1(1000,b,x)
            start = timeit.default_timer()
            bubbleSort(a) #this line was changed for each test to insertSort(a), selectionSort(a) etc
            end = timeit.default_timer()
            writer.writerow([b, end - start])
            b = 0.001 + b

nearlySortedTest()
testManually()
worstCaseTest()
