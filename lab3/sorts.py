import random
import math
import timeit
import csv

def final_sort(L):
    if len(L) <= 27:
        insertionSort(L)
    else:
        quad_pivot_quicksort(L)
    print(L)


def my_quicksort(L, type):
    if type == 1:
        copy = quicksort_copy(L)
    elif type == 2:
        copy = dual_pivot_quicksort(L)
    elif type == 3:
        copy = tri_pivot_quicksort(L)
    else:
        copy = quad_pivot_quicksort(L)

    for i in range(len(L)):
        L[i] = copy[i]
    #print(copy)

def quicksort_copy(L):
    if len(L) < 2:
        return L
    pivot = L[0]
    left, right = [], []
    for num in L[1:]:
        if num < pivot:
            left.append(num)
        else:
            right.append(num)
    return quicksort_copy(left) + [pivot] + quicksort_copy(right)

def dual_pivot_quicksort(list):
    x = len(list)
    if x <= 1:
        return list
    elif x == 2:
        return sorted(list)
    pivot1, pivot2 = sorted([list.pop(0), list.pop(0)])
    list1 = []
    list2 = []
    list3 = []
    for i in list:
        if i < pivot1:
            list1.append(i)
        elif pivot1 <= i < pivot2:
            list2.append(i)
        else:
            list3.append(i)
    return dual_pivot_quicksort(list1) + [pivot1] + dual_pivot_quicksort(list2) + [pivot2] + dual_pivot_quicksort(list3)


def create_random_list(n):
    L = []
    for _ in range(n):
        L.append(random.randint(1, n))
    return L

def create_descending_list(n):
    L = []
    for i in range(n-1,0,-1):
        L.append(i)
    return L

def create_near_sorted_list(n, factor):
    L = create_random_list(n)
    L.sort()
    for _ in range(math.ceil(n * factor)):
        index1 = random.randint(0, n - 1)
        index2 = random.randint(0, n - 1)
        L[index1], L[index2] = L[index2], L[index1]
    return L


def tri_pivot_quicksort(list):
    n = len(list)
    if n <= 1:
        return list
    elif n == 2:
        return sorted(list)
    pivot1, pivot2, pivot3 = sorted([list.pop(0), list.pop(0), list.pop(0)])
    list1 = []
    list2 = []
    list3 = []
    list4 = []
    for i in list:
        if i < pivot1:
            list1.append(i)
        elif pivot1 <= i < pivot2:
            list2.append(i)
        elif pivot2 <= i < pivot3:
            list3.append(i)
        else:
            list4.append(i)
    return tri_pivot_quicksort(list1) + [pivot1] + tri_pivot_quicksort(list2) + [pivot2] + tri_pivot_quicksort(
        list3) + [pivot3] + tri_pivot_quicksort(list4)


def quad_pivot_quicksort(list):
    n = len(list)
    if n <= 1:
        return list
    elif n == 2 or n == 3:
        return sorted(list)
    pivot1, pivot2, pivot3, pivot4 = sorted([list.pop(0), list.pop(0), list.pop(0), list.pop(0)])
    list1 = []
    list2 = []
    list3 = []
    list4 = []
    list5 = []
    for i in list:
        if i < pivot1:
            list1.append(i)
        elif pivot1 <= i < pivot2:
            list2.append(i)
        elif pivot2 <= i < pivot3:
            list3.append(i)
        elif pivot3 <= i < pivot4:
            list4.append(i)
        else:
            list5.append(i)
    return quad_pivot_quicksort(list1) + [pivot1] + quad_pivot_quicksort(list2) + [pivot2] + quad_pivot_quicksort(
        list3) + [pivot3] + quad_pivot_quicksort(list4) + [pivot4] + quad_pivot_quicksort(list5)


#my_quicksort([9, 8, 7, 6, 5])


def quicksort_inplace_driver(array, left, right):
    if left >= right:
        return
    l = left - 1
    pivot = array[right]
    for r in range(left, right):
        if pivot > array[r]:
            l += 1
            array[l], array[r] = array[r], array[l]
    array[l + 1], array[right] = array[right], array[l + 1]
    pivotIndex = l + 1
    quicksort_inplace_driver(array, left, pivotIndex - 1)
    quicksort_inplace_driver(array, pivotIndex + 1, right)

def quicksort_inplace(array):
    quicksort_inplace_driver(array, 0, len(array)-1)

def swap(L, i, j):
    temp = L[i]
    L[i] = L[j]
    L[j] = temp

def selectionSort(L):
    for i in range(len(L) - 1):
        mindex = get_min_index(L, i)
        swap(L, i, mindex)

def get_min_index(L, n):
    mindex = n

    for i in range(n, len(L)):
        if L[i] < L[mindex]:
            mindex = i

    return mindex

def insertionSort(L):
    for i in range(1, len(L)):
        insert_into(L, i)
    #print(L)

def insert_into(L, n):
    i = n
    swaps = 0
    while i > 0:
        #print(L)
        if L[i] < L[i - 1]:
            swap(L, i, i-1)
            swaps += 1
        else:
            return
        i -= 1

def timeTestInPlace(runs,L):
    total = 0
    for i in range(runs):
        start = timeit.default_timer()
        quicksort_inplace(L) #Manually change this for each test
        end = timeit.default_timer()
        total += end - start
    return total/runs

def timeTest(runs,L,type):
    total = 0
    for i in range(runs):
        start = timeit.default_timer()
        my_quicksort(L, type) #Manually change this for each test
        end = timeit.default_timer()
        total += end - start
    return total/runs

def testManually():

    with open('testingInPLace.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["n", "in place"])
        for i in range(100,1000,10):
            L = create_random_list(i)
            writer.writerow([i, timeTest(10,L)])

        return
"""
def runInOutPlace():

    with open('inoutplace.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["n", "out place", "in place"])
        for i in range(100,1000,10):
            L = create_random_list(i)
            writer.writerow([i,timeTest(10, my_quicksort(L, 1)), timeTest(10, quicksort_inplace(L))])

        return

def runQuicksortStandoff():

    with open('text.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["n", "single_pivot_quicksort", "dual_pivot_quicksort", "tri_pivot_quicksort", "quad_pivot_quicksort"])
        for i in range(100,1000,10):
            L = create_random_list(i)
            writer.writerow([i,timeTest(10, my_quicksort(L, 1)),timeTest(10, my_quicksort(L, 2)),timeTest(10,my_quicksort(L, 3)), timeTest(10,my_quicksort(L, 4))])

            #print(i, "{0} {1} {2}".format(timeTest(10, my_quicksort(L, 2)), timeTest(10,my_quicksort(L, 3)),
                                          #timeTest(10, my_quicksort(L, 4))))
        return

def runWorstVsAvg():
    with open('worstcomparison.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["n", "worst case", "average case"])
        for i in range(100,1000,10):
            L = create_random_list(i)
            L2 = create_descending_list(i)
            writer.writerow([i,timeTest(10, my_quicksort(L2, 1)), timeTest(10, my_quicksort(L,1))])

        return
"""