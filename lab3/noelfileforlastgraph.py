import random
import math
import timeit
import csv

def insertionSort(arr):
    for i in range(1, len(arr)): 
        key = arr[i] 
        j = i-1
        while j >= 0 and key < arr[j] : 
                arr[j + 1] = arr[j] 
                j -= 1
        arr[j + 1] = key
    return arr

def quad_pivot_quicksort(list):
    n = len(list)
    if n <= 1:
        return list
    elif n == 2 or n == 3:
        return sorted(list)
    pivot1, pivot2, pivot3, pivot4 = sorted([list.pop(0), list.pop(0), list.pop(0), list.pop(0)])
    list1 = []
    list2 = []
    list3 = []
    list4 = []
    list5 = []
    for i in list:
        if i < pivot1:
            list1.append(i)
        elif pivot1 <= i < pivot2:
            list2.append(i)
        elif pivot2 <= i < pivot3:
            list3.append(i)
        elif pivot3 <= i < pivot4:
            list4.append(i)
        else:
            list5.append(i)
    return quad_pivot_quicksort(list1) + [pivot1] + quad_pivot_quicksort(list2) + [pivot2] + quad_pivot_quicksort(
        list3) + [pivot3] + quad_pivot_quicksort(list4) + [pivot4] + quad_pivot_quicksort(list5)

def bubbleSort(arr): 
    n = len(arr) 
    for i in range(n): 
        for j in range(0, n-i-1): 
            if arr[j] > arr[j+1] : 
                arr[j], arr[j+1] = arr[j+1], arr[j]

def selectionSort(A):
    for i in range(len(A)): 
        min_idx = i 
        for j in range(i+1, len(A)): 
            if A[min_idx] > A[j]: 
                min_idx = j         
        A[i], A[min_idx] = A[min_idx], A[i]

def create_near_sorted_list(n, factor,L):
    L.sort()
    for _ in range(math.ceil(len(L) * factor)):
        index1 = random.randint(0, len(L) - 1)
        index2 = random.randint(0, len(L) - 1)
        L[index1], L[index2] = L[index2], L[index1]
    return L

def test():
    with open('tttttooooddd555.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["factor", "Run time"])
        #totalTime = 0
        x = random.sample(range(1000), 1000)
        b = 0
        for i in range(1,100):
            a = create_near_sorted_list(1000,b,x)
            start = timeit.default_timer()
            bubbleSort(a)
            end = timeit.default_timer()
            writer.writerow([b, end - start])
            b = 0.001 + b

test()
