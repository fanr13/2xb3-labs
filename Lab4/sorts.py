def mergesort_three_driver(L, l, h):
    if len(L[l : h]) < 2:
        return L
    else:
        mid1 = l + ((h - l) // 3)
        mid2 = l + 2 * ((h - l) // 3) + 1
        mergesort_three_driver(L, l, mid1)
        mergesort_three_driver(L, mid1, mid2)
        mergesort_three_driver(L, mid2, h)
        merge_three(L, l, mid1, mid2, h)
    return L

 def merge_three(L, l, mid1, mid2, h):
    lower = L[l : mid1]
    mid = L[mid1: mid2]
    upper = L[mid2 : h]
    lower.append(float('inf'))
    mid.append(float('inf'))
    upper.append(float('inf'))
    x = y = z = 0
    for i in range(l, h):
        minimum = min([lower[x], mid[y], upper[z]])
        if minimum == lower[x]:
            L[i] = lower[x]
            x += 1
        elif minimum == mid[y]:
            L[i] = mid[y]
            y += 1
        else:
            L[i] = upper[z]
            z += 1 

def mergesort_three(L):
    temp = mergesort_three_driver(L, 0, len(L))
    for i in range(len(temp)):
        L[i] = temp[i]

def mergesort_bottom(L): 
    current_size = 1
    while current_size < len(L):
        start = 0
        while start < len(L)-1: 
            mid = min((start + current_size - 1),(len(L)-1))
            end = ((2 * current_size + start - 1, len(L) - 1)[2 * current_size + start - 1 > len(L)-1]) 
            merge_bottom(L, start, mid, end) 
            start = start + current_size*2
        current_size = 2 * current_size 

def merge_bottom(L, start, mid, end): 
    n1 = mid - start + 1
    n2 = end - mid 
    Left = [0] * n1 
    right = [0] * n2 
    for i in range(0, n1): 
        Left[i] = L[start + i] 
    for i in range(0, n2): 
        right[i] = L[mid + i + 1] 
 
    i, j, k = 0, 0, start 
    while i < n1 and j < n2: 
        if Left[i] > right[j]: 
            L[k] = right[j] 
            j += 1
        else: 
            L[k] = Left[i] 
            i += 1
        k += 1
 
    while i < n1: 
        L[k] = Left[i] 
        i += 1
        k += 1

    while j < n2: 
        L[k] = right[j] 
        j += 1
        k += 1
