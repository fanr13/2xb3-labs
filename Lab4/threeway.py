def mergesort_three_driver(arr, l, h):
    if len(arr[l-1 : h]) < 2:
        return arr
    else:
        mid1 = l + ((h - l) // 3) 
        mid2 = l + 2 * ((h-l) // 3)

        mergesort_three_driver(arr, l, mid1) 
        mergesort_three_driver(arr, mid1+1, mid2 + 1)
        mergesort_three_driver(arr, mid2+2, h) 
        merge_three(arr, l, mid1, mid2, h) 
    return arr 

def merge_three(arr, l, mid1, mid2, h):

    left = arr[l-1 : mid1]
    mid = arr[mid1: mid2+1]
    right = arr[mid2+1 : h]
    left.append(float('inf'))
    mid.append(float('inf'))
    right.append(float('inf'))
    x = 0 
    y = 0   
    z = 0 

    for i in range(l-1, h): 
        minimum = min([left[x], mid[y], right[z]]) 
        if minimum == left[x]:
            arr[i] = left[x]
            x += 1 
        elif minimum == mid[y]:
            arr[i] = mid[y]
            y += 1 
        else:
            arr[i] = right[z]
            z += 1  

def mergesort_three(L):
    return mergesort_three_driver(L, 1, len(L))
