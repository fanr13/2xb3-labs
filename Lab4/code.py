import random
import math
import timeit
import csv


def mergesort_bottom(L):
    current_size = 1
    while current_size < len(L):
        start = 0
        while start < len(L) - 1:
            mid = min((start + current_size - 1), (len(L) - 1))
            end = ((2 * current_size + start - 1, len(L) - 1)[2 * current_size + start - 1 > len(L) - 1])
            merge_bottom(L, start, mid, end)
            start = start + current_size * 2
        current_size = 2 * current_size

def merge_bottom(L, start, mid, end):
    n1 = mid - start + 1
    n2 = end - mid
    Left = [0] * n1
    right = [0] * n2
    for i in range(0, n1):
        Left[i] = L[start + i]
    for i in range(0, n2):
        right[i] = L[mid + i + 1]

    i, j, k = 0, 0, start
    while i < n1 and j < n2:
        if Left[i] > right[j]:
            L[k] = right[j]
            j += 1
        else:
            L[k] = Left[i]
            i += 1
        k += 1

    while i < n1:
        L[k] = Left[i]
        i += 1
        k += 1

    while j < n2:
        L[k] = right[j]
        j += 1
        k += 1

def mergesort(L):
    if len(L) <= 1:
        return
    mid = len(L) // 2
    left, right = L[:mid], L[mid:]

    # Mergesort core
    mergesort(left)
    mergesort(right)
    temp = merge(left, right)

    # Copy the sorted list to L
    for i in range(len(temp)):
        L[i] = temp[i]


def merge(left, right):
    L = []
    i = j = 0

    while i < len(left) or j < len(right):
        # Check it there's still elements to be merged from left and/or right
        if i >= len(left):
            L.append(right[j])
            j += 1
        elif j >= len(right):
            L.append(left[i])
            i += 1
        else:
            if left[i] <= right[j]:
                L.append(left[i])
                i += 1
            else:
                L.append(right[j])
                j += 1
    return L

def mergesort_three_driver(L, l, h):
    if len(L[l : h]) < 2:
        return L
    else:
        mid1 = l + ((h - l) // 3) 
        mid2 = l + 2 * ((h - l) // 3) + 1
        mergesort_three_driver(L, l, mid1) 
        mergesort_three_driver(L, mid1, mid2)
        mergesort_three_driver(L, mid2, h) 
        merge_three(L, l, mid1, mid2, h) 
    return L
def merge_three(L, l, mid1, mid2, h):

    lower = L[l : mid1]
    mid = L[mid1: mid2]
    upper = L[mid2 : h]
    lower.append(float('inf'))
    mid.append(float('inf'))
    upper.append(float('inf'))
    x = y = z = 0

    for i in range(l, h): 
        minimum = min([lower[x], mid[y], upper[z]]) 
        if minimum == lower[x]:
            L[i] = lower[x]
            x += 1 
        elif minimum == mid[y]:
            L[i] = mid[y]
            y += 1 
        else:
            L[i] = upper[z]
            z += 1  

def mergesort_three(L):
    temp = mergesort_three_driver(L, 0, len(L))
    for i in range(len(temp)):
        L[i] = temp[i]


def create_near_sorted_list(n, factor):
    L = create_random_list(n)
    L.sort()
    for _ in range(math.ceil(n * factor)):
        index1 = random.randint(0, n - 1)
        index2 = random.randint(0, n - 1)
        L[index1], L[index2] = L[index2], L[index1]
    return L


def create_random_list(n):
    L = []
    for _ in range(n):
        L.append(random.randint(1, n))
    return L

def test1():
    l = []
    for i in range(10):
        l.append(random.randint(0,20))
    print(l)
    mergesort_bottom(l)
    print(l)

def timeTestMerge(runs,L):
    total = 0
    for i in range(runs):
        start = timeit.default_timer()
        mergesort(L)
        end = timeit.default_timer()
        total += end - start
    return total/runs

def timeTestMergeBU(runs,L):
    total = 0
    for i in range(runs):
        start = timeit.default_timer()
        mergesort_bottom(L)
        end = timeit.default_timer()
        total += end - start
    return total/runs


def test_mergeBU():
    with open('mergeBU.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["n", "merge", "mergeBU"])
        # totalTime = 0
        for i in range(100,10100,100):
            #x = random.sample(range(0,10000,100), i)
            x = create_random_list(i)
            start1 = timeit.default_timer()
            mergesort(x)
            end1 = timeit.default_timer()

            start2 = timeit.default_timer()
            mergesort_bottom(x)
            end2 = timeit.default_timer()
            writer.writerow([i, end1 - start1, end2 - start2])


#test_mergeBU()

def test_2vs3_way():
    with open('3waymerge.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["n", "2 way merge", "3 way merge"])
        for i in range(100,10100,100):
            tot1 = 0
            tot2 = 0
            for _ in range(5):
                x = create_random_list(i)
                start1 = timeit.default_timer()
                mergesort(x)
                end1 = timeit.default_timer()
                tot1 += end1 - start1

                start2 = timeit.default_timer()
                mergesort_three(x)
                end2 = timeit.default_timer()
                tot2 += end2 - start2
            writer.writerow([i, tot1/5, tot2/5])

#test_2vs3_way()

def test_nearly_sorted():
    with open('factormerge.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["n", "nearlysorted"])
        # totalTime = 0
        for i in range(51):
            tot = 0
            for j in range(5):
                y = create_near_sorted_list(10000, 0.01*i)
                start2 = timeit.default_timer()
                mergesort(y)
                end2 = timeit.default_timer()
                tot += end2 - start2
            writer.writerow([i*0.01, tot/5])

#test_nearly_sorted()
