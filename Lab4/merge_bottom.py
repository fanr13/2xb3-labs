def mergesort_bottom(L): 
    current_size = 1
    while current_size < len(L):
        start = 0
        while start < len(L)-1: 
            mid = min((start + current_size - 1),(len(L)-1))
            end = ((2 * current_size + start - 1, len(L) - 1)[2 * current_size + start - 1 > len(L)-1]) 
            merge_bottom(L, start, mid, end) 
            start = start + current_size*2
        current_size = 2 * current_size 

def merge_bottom(L, start, mid, end): 
    n1 = mid - start + 1
    n2 = end - mid 
    Left = [0] * n1 
    right = [0] * n2 
    for i in range(0, n1): 
        Left[i] = L[start + i] 
    for i in range(0, n2): 
        right[i] = L[mid + i + 1] 
 
    i, j, k = 0, 0, start 
    while i < n1 and j < n2: 
        if Left[i] > right[j]: 
            L[k] = right[j] 
            j += 1
        else: 
            L[k] = Left[i] 
            i += 1
        k += 1
 
    while i < n1: 
        L[k] = Left[i] 
        i += 1
        k += 1

    while j < n2: 
        L[k] = right[j] 
        j += 1
        k += 1