import timeit
import csv
import os

# This code uses csv python support and plotted with Excel

def append():
    with open('append.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["n", "Run time"])
        totalTime = 0
        list = []
        for i in range(1000000):
            start = timeit.default_timer()
            list.append(1)
            end = timeit.default_timer()
            totalTime += start - end
            writer.writerow([i, end - start])

    return totalTime / 1000000


def copy():
    with open('copy.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["n", "Run time"])
        totalTime = 0

        for i in range(10000):
            x = [0] * i
            start = timeit.default_timer()
            x.copy()
            end = timeit.default_timer()
            totalTime += end - start
            writer.writerow([i, end - start])
    return totalTime / 10000


def lookup():
    with open('lookup.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["n", "Run time"])
        totalTime = 0
        x = [0] * 1000000
        for i in range(1, 1000001):
            start = timeit.default_timer()
            y = x[i - 1]
            end = timeit.default_timer()
            totalTime += end - start
            writer.writerow([i, end - start])
    return totalTime / 1000000


def test():
    print(append())
    print(copy())
    print(lookup())


test()

