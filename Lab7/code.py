import random
import math
import timeit
import csv
from graphs import *


def test_graph1():
    with open('9cwgraph3970e5r47000007777.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["c", "ans"])
        for i in range(300):
            #x = create_random_list(i)
            #x = random.sample(range(100), i)
            #print(x)
            t = 0
            for j in range(100):
                G = Graph(100)
                for x in range(i):
                    a = random.randrange(100)
                    b = random.randrange(100)
                    while(a == b or G.are_connected(a,b)):
                        a = random.randrange(100)
                        b = random.randrange(100)
                    G.add_edge(a,b)
                if has_cycle(G):
                    t += 1
            writer.writerow([i, t])

def test_graph2():
    with open('999cwgraph3970e5r47000007777.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["c", "ans"])
        for i in range(500):
            #x = create_random_list(i)
            #x = random.sample(range(100), i)
            #print(x)
            t=0
            for j in range(100):
                G = Graph(100)
                for x in range(i):
                    a = random.randrange(100)
                    b = random.randrange(100)
                    while(a == b or G.are_connected(a,b)):
                        a = random.randrange(100)
                        b = random.randrange(100)
                    G.add_edge(a,b)
                if is_connected(G):
                    t +=1
            writer.writerow([i, t])

test_graph1()
test_graph2()