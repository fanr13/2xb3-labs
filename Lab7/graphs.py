from collections import deque

#Undirected graph using an adjacency list
class Graph:

    def __init__(self, n):
        self.adj = {}
        for i in range(n):
            self.adj[i] = []

    def are_connected(self, node1, node2):
        return node2 in self.adj[node1]

    def adjacent_nodes(self, node):
        return self.adj[node]

    def add_node(self):
        self.adj[len(self.adj)] = []

    def add_edge(self, node1, node2):
        if node1 not in self.adj[node2]:
            self.adj[node1].append(node2)
            self.adj[node2].append(node1)

    def number_of_nodes(self):
        return len(self.adj)


#Breadth First Search
def BFS(G, node1, node2):
    Q = deque([node1])
    marked = {node1 : True}
    for node in G.adj:
        if node != node1:
            marked[node] = False
    while len(Q) != 0:
        current_node = Q.popleft()
        for node in G.adj[current_node]:
            if node == node2:
                return True
            if not marked[node]:
                Q.append(node)
                marked[node] = True
    return False


#Depth First Search
def DFS(G, node1, node2):
    S = [node1]
    marked = {}
    for node in G.adj:
        marked[node] = False
    while len(S) != 0:
        current_node = S.pop()
        if not marked[current_node]:
            marked[current_node] = True
            for node in G.adj[current_node]:
                if node == node2:
                    return True
                S.append(node)
    return False

def BFS2(G, node1, node2):
    x = []
    x.append(node1)
    Q = deque([node1])
    marked = {node1 : True}
    for node in G.adj:
        if node != node1:
            marked[node] = False
    while len(Q) != 0:
        current_node = Q.popleft()
        for node in G.adj[current_node]:
            if node == node2:
                if node not in x: x.append(node)
                if node == node1 : return [node] 
                return x
            if not marked[node]:
                Q.append(node)
                marked[node] = True
                if node not in x: x.append(node) 
    return []

def DFS2(G, node1, node2):
    x = []
    x.append(node1)
    S = [node1]
    marked = {}
    for node in G.adj:
        marked[node] = False
    while len(S) != 0:
        current_node = S.pop()
        if not marked[current_node]:
            marked[current_node] = True
            for node in G.adj[current_node]:
                if node == node2:
                    if node not in x: x.append(node)
                    if node == node1 : return [node]
                    return x
                if node not in x: x.append(node) 
                S.append(node)
    return []

def BFS3(G, node1):
    Q = deque([node1])
    marked = {node1 : True}
    parent = {}
    for node in G.adj:
        if node != node1:
            marked[node] = False
    while len(Q) != 0:
        current_node = Q.popleft()
        for node in G.adj[current_node]:
            if node not in parent and node != node1:
                parent[node] = current_node
            if not marked[node]:
                Q.append(node)
                marked[node] = True
    return parent

def DFS3(G, node1):
    S = [node1]
    marked = {}
    parent = {}
    for node in G.adj:
        marked[node] = False
    while len(S) != 0:
        current_node = S.pop()
        if not marked[current_node]:
            marked[current_node] = True
            for node in G.adj[current_node]:
                if node not in parent and node != node1:
                    parent[node] = current_node
                S.append(node)
    return parent

def has_cycle(G):
    #list to keep track of visited ndoes

    visited = [False]*(G.number_of_nodes())

    for i in range(G.number_of_nodes()):
        if visited[i] == False:
            if has_cycle_driver(G,i,visited, -1) == True:
                return True

    return False

def has_cycle_driver(G, neighbor, visited, parent):

    visited[neighbor] = True

    for i in G.adj[neighbor]:
        if visited[i] == False:
            if has_cycle_driver(G, i, visited, neighbor):
                return True
        elif parent != i:
            return True

    return False

def is_connected(G):
    return is_connected_driver(G, 0)

def is_connected_driver(G, start):
    Q = deque([start])

    #list to keep track of visited ndoes
    visited = [False] * G.number_of_nodes()
    visited[start] = True

    while(len(Q) != 0):
        node = Q.pop()
        neighbors = G.adj[node]

        for neighbor in neighbors:
            if(visited[neighbor] == False):
                Q.append(neighbor)
                visited[neighbor] = True

    return all(visited)



def test_Jeremy():

    """
    G1a visual
    0   1 - 2 - 3 - 4 - 5 - 6
        |                   |
        - - - - - - - - - - -
    """
    G1a = Graph(7)
    #G1.add_edge(0,1)
    G1a.add_edge(1,2)
    G1a.add_edge(2,3)
    G1a.add_edge(3,4)
    G1a.add_edge(4,5)
    G1a.add_edge(5,6)
    G1a.add_edge(3,6)
    print("G1a is connected: " + str(is_connected(G1a)))
    print("G1a is has a cycle: " + str(has_cycle(G1a)))
    print(" ")

    """
        G1b visual
        0 - 1 - 2 - 3 - 4 - 5 - 6
            |                   |
            - - - - - - - - - - -        
        """
    G1b = Graph(7)
    G1b.add_edge(0,1)
    G1b.add_edge(1, 2)
    G1b.add_edge(2, 3)
    G1b.add_edge(3, 4)
    G1b.add_edge(4, 5)
    G1b.add_edge(5, 6)
    G1b.add_edge(3, 6)
    print("G1b is connected: " + str(is_connected(G1b)))
    print("G1b is has a cycle: " + str(has_cycle(G1b)))
    print(" ")

    """
    G2a visual 
    0 - 1 - 2 - 3 - 4 - 5 - 6
    """
    G2a = Graph(7)
    G2a.add_edge(0, 1)
    G2a.add_edge(1, 2)
    G2a.add_edge(2, 3)
    G2a.add_edge(3, 4)
    G2a.add_edge(4, 5)
    G2a.add_edge(5, 6)
    print("G2a is connected: " + str(is_connected(G2a)))
    print("G2a is has a cycle: " + str(has_cycle(G2a)))
    print(" ")

    """
        G2b visual 
        0 - 1 - 2    3 - 4 - 5 - 6
        """
    G2b = Graph(7)
    G2b.add_edge(0, 1)
    G2b.add_edge(1, 2)
    G2b.add_edge(3, 4)
    G2b.add_edge(4, 5)
    G2b.add_edge(5, 6)
    print("G2b is connected: " + str(is_connected(G2b)))
    print("G2b is has a cycle: " + str(has_cycle(G2b)))
    print(" ")

    """
            G2c visual 
            0 - 1 - 2    3 - 4 - 5 - 6
            |       |
            7 - 8 - 9
            """
    G2c = Graph(10)
    G2c.add_edge(0, 1)
    G2c.add_edge(1, 2)
    G2c.add_edge(3, 4)
    G2c.add_edge(4, 5)
    G2c.add_edge(5, 6)
    G2c.add_edge(0, 7)
    G2c.add_edge(7, 8)
    G2c.add_edge(8, 9)
    G2c.add_edge(9, 2)
    print("G2c is connected: " + str(is_connected(G2c)))
    print("G2c is has a cycle: " + str(has_cycle(G2c)))


#test_Jeremy()

G = Graph(7)
G.add_edge(1,2)
G.add_edge(1,3)
G.add_edge(2,4)
G.add_edge(3,4)
G.add_edge(3,5)
G.add_edge(4,5)
G.add_edge(4,6)
print(DFS2(G,2,4))
print(DFS3(G,1))
print(BFS3(G,1))