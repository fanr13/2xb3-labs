import math

class k_heap:

    length = 0
    data = []

    def __init__(self, values, k):
        if len(values) == 0:
            self.data = []
            self.k = k
            return
        self.data = values
        self.length = len(values)
        self.k = k
        self.init_heap()

    def _insert(self, value):
        self.data.insert(0, value)
        self.length += 1
        self.sink(0)

    def init_heap(self):
        temp = self.data
        tempLen = self.length
        self.data = []
        self.length = 0
        for i in range(tempLen):
            self._insert(temp[i])

    def build_heap(self, values):
        for i in range(len(values)):
            self._insert(values[i])

    def parent(self, index):
        if self.length == 1:
            return None
        return math.floor(abs(index - 1) // self.k)

    def children(self, i):
        l = []
        for j in range(1, self.k + 1):
            if self.length > i * self.k + j:
                l.append(self.data[i * self.k + j])
        return l

    def get_root(self):
        if self.data != []:
            return self.data[0]
        return None

    def sink(self, i):
        largest_known = i
        if self.children(i) != []:
            if max(self.children(i)) > self.data[i]:
                largest_known = max(self.children(i))

        if largest_known != i:
            self.data[i], self.data[largest_known] = self.data[largest_known], self.data[i]
            self.sink(largest_known)


def main():

    L = [1,2,3,4,5,6,8,10]
    k = k_heap(L,3)
    #k = k_Heap([],5)
    #k.build_heap([5,6,7])

    print("root is " + str(k.get_root()))
    print("children of root are " + str(k.children(0)))
    print("children of index 1 are " + str(k.children(1)))
    print("children of index 2 are " + str(k.children(2)))
    print("parent of index i are " + str(k.parent(3)))

main()