import random
import math
import timeit
import csv
from heap import *

def create_random_list(n):
    return [random.random() for _ in range(n)]


def test_heap():
    with open('heap012.csv', 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["n", "run time"])
        for i in range(1,5000):
            x = create_random_list(i)
            start1 = timeit.default_timer()
            Heap(x)
            end1 = timeit.default_timer()
            writer.writerow([i, end1 - start1])

test_heap()