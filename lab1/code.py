def are_valid_groups(list1,t):
    for i in range(len(t)-1):
        if (len(t[i]) == 2 or len(t[1]) == 3):
            continue
        else:
            return False
    flat_list = [item for sublist in t for item in sublist]


    if(len(flat_list) != len(set(flat_list))):
        return False
    return set(list1).issubset(flat_list)


