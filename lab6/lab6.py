class RBNode:

    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None
        self.parent = None
        self.colour = "R"

    # def get_uncle(self):
    #    return

    def is_leaf(self):
        return self.left == None and self.right == None

    def is_left_child(self):
        return self == self.parent.left

    def is_right_child(self):
        return not self.is_left_child()

    def is_red(self):
        return self.colour == "R"

    def is_black(self):
        return not self.is_red()

    def make_black(self):
        self.colour = "B"

    def make_red(self):
        self.colour = "R"

    def get_brother(self):
        if self.parent.right == self:
            return self.parent.left
        return self.parent.right

    def get_uncle(self):
        return self.parent.get_brother()

    def uncle_is_black(self):
        if self.get_uncle() == None:
            return True
        return self.get_uncle().is_black()

    def __str__(self):
        return "(" + str(self.value) + "," + self.colour + ")"

    def __repr__(self):
        return "(" + str(self.value) + "," + self.colour + ")"

    def rotate_right(self):
        y = self.value.left
        self.value.left = y.right
        if y.right != self.root:
            y.right.parent = self.value

        y.parent = self.value.parent
        if self.value.parent == None:
            self.root = y
        elif self.value == self.value.parent.right:
            self.value.parent.right = y
        else:
            self.value.parent.left = y
        y.right = self.value
        self.value.parent = y

    def rotate_left(self):
        y = self.value.right
        self.value.right = y.left
        if y.left != self.root:
            y.left.parent = self.value

        y.parent = self.value.parent
        if self.value.parent == None:
            self.root = y
        elif self.value == self.value.parent.left:
            self.value.parent.left = y
        else:
            self.value.parent.right = y
        y.left = self.value
        self.value.parent = y


class RBTree:

    def __init__(self):
        self.root = None

    def is_empty(self):
        return self.root == None

    def get_height(self):
        if self.is_empty():
            return 0
        return self.__get_height(self.root)

    def __get_height(self, node):
        if node == None:
            return 0
        return 1 + max(self.__get_height(node.left), self.__get_height(node.right))

    def insert(self, value):
        if self.is_empty():
            self.root = RBNode(value)
            self.root.make_black()
        else:
            self.__insert(self.root, value)

    def __insert(self, node, value):
        if value < node.value:
            if node.left == None:
                node.left = RBNode(value)
                node.left.parent = node
                self.fix(node.left)
            else:
                self.__insert(node.left, value)
        else:
            if node.right == None:
                node.right = RBNode(value)
                node.right.parent = node
                self.fix(node.right)
            else:
                self.__insert(node.right, value)

    def fix(self, node):

        # You may alter code in this method if you wish, it's merely a guide.
        if node.parent == None:
            node.make_black()
        while node != None and node.parent != None and node.parent.is_red():
            if node.parent.is_black():
                return
            if node.parent.parent.right == node.parent:
                if not node.uncle_is_black():
                    node.parent.parent.left.make_black()
                    node.parent.parent.make_red()
                    node.parent.make_black()
                    node = node.parent.parent
                else:
                    if node == node.parent.left:
                        node = node.parent
                        node.rotate_right()
                    node.parent.make_black()
                    node.parent.parent.make_red()
                    node.parent.parent.rotate_left()
            else:
                if not node.uncle_is_black():
                    node.parent.parent.right.make_black()
                    node.parent.make_black()
                    node.parent.parent.make_red()
                    node = node.parent.parent
                else:
                    if node == node.parent.right:
                        node = node.parent
                        node.rotate_left()
                    node.parent.make_black()
                    node.parent.parent.make_red()
                    node.parent.parent.rotate_right()

        self.root.make_black()

    def __str__(self):
        if self.is_empty():
            return "[]"
        return "[" + self.__str_helper(self.root) + "]"

    def __str_helper(self, node):
        if node.is_leaf():
            return "[" + str(node) + "]"
        if node.left == None:
            return "[" + str(node) + " -> " + self.__str_helper(node.right) + "]"
        if node.right == None:
            return "[" + self.__str_helper(node.left) + " <- " + str(node) + "]"
        return "[" + self.__str_helper(node.left) + " <- " + str(node) + " -> " + self.__str_helper(node.right) + "]"


rbtree = RBTree()
rbtree.insert(6)
rbtree.insert(5)
rbtree.insert(6)
rbtree.insert(87)
rbtree.insert(52)
# rbtree.insert(1)
# rbtree.insert(2)
print(rbtree)

